package pe.com.baufest;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import pe.com.baufest.model.Category;
import pe.com.baufest.model.PetData;
import pe.com.baufest.question.GetPetQuestion;
import pe.com.baufest.question.ResponseStatusCode;
import pe.com.baufest.task.GetPet;
import pe.com.baufest.task.PostPet;

import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;


@RunWith(SerenityRunner.class)
public class RunnerTestLocal {
    private final String restApiUrl="https://petstore.swagger.io";

    @Test
    public void testServiceGet(){

        Long idPet = 9223372000666053513L ;

        Actor Gino = Actor.named("Gino")
                .whoCan(CallAnApi.at(restApiUrl));

        Gino.attemptsTo(
                GetPet.petIdParameter(idPet)
        );

        PetData petData = new GetPetQuestion().answeredBy(Gino);

        Gino.should(
                seeThat("Status Code", ResponseStatusCode.statusCode(),equalTo(HttpStatus.SC_OK))
        );

        Gino.should(
                seeThat("Usuario no null", actor -> petData,notNullValue() )
        );

        System.out.println(petData.getId());

        Gino.should(
                seeThat("id de la mascota", actor -> petData.getId(),equalTo(idPet))
        );
    }

    @Test
    public void testServicePost(){
        Actor Gino = Actor.named("Gino")
                .whoCan(CallAnApi.at(restApiUrl));

        String petData = "{\n" +
                "  \"id\": 0,\n" +
                "  \"category\": {\"id\": 0,\"name\": \"string\" },\n" +
                "  \"name\": \"doggie\",\n" +
                "  \"photoUrls\": [\"string\"],\n" +
                "  \"tags\": [{\"id\": 0,\"name\": \"string\"}],\n" +
                "  \"status\": \"available\"\n" +
                "}";

        Gino.attemptsTo(
                PostPet.petRegisterForms(petData)
        );

        Gino.should(
                seeThat("Status Code", ResponseStatusCode.statusCode(),equalTo(HttpStatus.SC_OK))
        );
    }
}
