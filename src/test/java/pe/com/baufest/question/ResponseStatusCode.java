package pe.com.baufest.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ResponseStatusCode implements Question {

    public static Question<Integer> statusCode(){
        return new ResponseStatusCode();
    }

    @Override
    public Integer answeredBy(Actor actor) {
       return SerenityRest.lastResponse().statusCode();
    }
}
