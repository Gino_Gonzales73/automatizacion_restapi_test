package pe.com.baufest.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import pe.com.baufest.model.PetData;

public class GetPetQuestion implements Question {

    @Override
    public PetData answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(PetData.class);
    }
}
