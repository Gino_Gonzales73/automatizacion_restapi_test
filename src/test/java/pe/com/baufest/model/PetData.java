package pe.com.baufest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PetData {

    public Long id;
    public Category category;
    public String name;
    public List<String> photoUrls ;
    public List<Tag> tags ;
    public String status;
}
