package pe.com.baufest.stepdefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import org.apache.http.HttpStatus;
import pe.com.baufest.model.PetData;
import pe.com.baufest.question.GetPetQuestion;
import pe.com.baufest.question.ResponseStatusCode;
import pe.com.baufest.task.GetPet;
import pe.com.baufest.task.PostPet;
import pe.com.baufest.task.PutPet;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class StepCrudPet {

    private final String restApiPed = "https://petstore.swagger.io";
    Actor Gino;
    private Long idPet = 105020L;

    @Given("^Gino como administrador quiera probar un CRUD$")
    public void rest_gino_initial_server(){
        Gino = Actor.named("Gino es Administrador")
                .whoCan(CallAnApi.at(restApiPed));
    }

    @When("^Registra una nueva mascota$")
    public void rest_gino_register_pet(){
        //ESTO SE PUEDE MANEJAR CON OBJETOS INSTANCIANDO Y ENVIANDO VALORES
        String petRegisterData = "{\n" +
                "  \"id\": "+idPet+",\n" +
                "  \"category\": {\"id\": 0,\"name\": \"Pequeños\" },\n" +
                "  \"name\": \"Jacob\",\n" +
                "  \"photoUrls\": [\"string\"],\n" +
                "  \"tags\": [{\"id\": 0,\"name\": \"Orejas Grandes\"}],\n" +
                "  \"status\": \"available\"\n" +
                "}";

        Gino.attemptsTo(
                PostPet.petRegisterForms(petRegisterData)
        );
    }

    @And("^actualiza sus datos$")
    public void rest_gino_update_pet(){
        //ESTO SE PUEDE MANEJAR CON OBJETOS INSTANCIANDO Y ENVIANDO VALORES
        String petUpdateData = "{\n" +
                "  \"id\": "+idPet+",\n" +
                "  \"category\": {\"id\": 0,\"name\": \"Grandes\" },\n" +
                "  \"name\": \"Lucas\",\n" +
                "  \"photoUrls\": [\"string\"],\n" +
                "  \"tags\": [{\"id\": 0,\"name\": \"Orejas Pequeñas\"}],\n" +
                "  \"status\": \"available\"\n" +
                "}";

        Gino.attemptsTo(
                PutPet.petUpdateForms(petUpdateData)
        );
    }

    @And("^Consulto para obtener el cambio$")
    public void rest_gino_get_pet(){
        //AVECES EL SERVICIO RETORNA EL DATO ACTUALIZADO EN LA SEGUNDA CONSULTA
        for (int i = 0; i < 2;i++){
            Gino.attemptsTo(
                    GetPet.petIdParameter(idPet)
            );
        }
    }

    @Then("^Se visualizara los datos de la mascota con la ultima actualizacion$")
    public void rest_gino_assets_response(){
        PetData petData = new GetPetQuestion().answeredBy(Gino);

        Gino.should(
                seeThat("Status Code", ResponseStatusCode.statusCode(),equalTo(HttpStatus.SC_OK))
        );
        Gino.should(
                seeThat("Usuario no null", actor -> petData,notNullValue() )
        );
        Gino.should(
                seeThat("id de la mascota", actor -> petData.getId(),equalTo(idPet)),
                seeThat("El nombre es",actor -> petData.getName(),equalTo("Lucas"))
        );
    }
}
