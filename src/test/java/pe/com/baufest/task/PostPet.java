package pe.com.baufest.task;

import io.restassured.http.ContentType;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Put;
import pe.com.baufest.interactions.Post;

import static net.serenitybdd.screenplay.Tasks.instrumented;

@AllArgsConstructor
public class PostPet implements Task {

    private final String petForms;

    public static Performable petRegisterForms(String petForms){
        return instrumented(PostPet.class,petForms);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("/v2/pet").with(
                        requestSpecification -> requestSpecification
                                .contentType(ContentType.JSON)
                                .body(petForms)
                )
        );
    }
}
