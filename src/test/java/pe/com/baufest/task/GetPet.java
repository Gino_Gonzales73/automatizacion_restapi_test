package pe.com.baufest.task;

import io.restassured.http.ContentType;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static net.serenitybdd.screenplay.Tasks.instrumented;

@AllArgsConstructor
public class GetPet implements Task {

    private final Long idPet;

    public static Performable petIdParameter(Long idPet){
        return instrumented(GetPet.class,idPet);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/v2/pet/"+idPet).with(
                        requestSpecification -> requestSpecification
                        .contentType(ContentType.JSON))
        );
    }
}
