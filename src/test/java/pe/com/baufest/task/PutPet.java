package pe.com.baufest.task;

import io.restassured.http.ContentType;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import pe.com.baufest.interactions.Post;
import pe.com.baufest.interactions.Put;

import static net.serenitybdd.screenplay.Tasks.instrumented;

@AllArgsConstructor
public class PutPet implements Task {

    private final String petForms;

    public static Performable petUpdateForms(String petForms){
        return instrumented(PutPet.class,petForms);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Put.to("/v2/pet").with(
                        requestSpecification -> requestSpecification
                                .contentType(ContentType.JSON)
                                .body(petForms)
                )
        );
    }
}
