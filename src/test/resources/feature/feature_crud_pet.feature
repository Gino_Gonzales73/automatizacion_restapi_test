@Service_ApiRest
Feature: Registro,Obtener y Actualizar datos de una mascota
  Yo como administrador del servicio
  Quiero realizar un registro, actualizacion y obtecion de datos de una mascota
  para ver que mi servicio responda ok

  Scenario: realizar un CRUD a nivel de servicio
    Given Gino como administrador quiera probar un CRUD
    When Registra una nueva mascota
    And actualiza sus datos
    And Consulto para obtener el cambio
    Then Se visualizara los datos de la mascota con la ultima actualizacion