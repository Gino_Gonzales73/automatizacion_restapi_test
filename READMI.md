>Este proyecto, tiene el fin de poderse usar para pruebas de servicio.
>

**para poder correr la prueba elaborada dentro del proyecto**

>
>Desarrollado con gradle 6.9
>
>Desarrollado con JDK 8
>

    -- PARA EJECUTAR SOLO INGRESAR EL SIGUIENTE COMANDO EN LA TERMINAL
    
    -- Ejecutar en la terminal de manera directa
    -- gradle clean test --tests "pe.com.baufest.RunnerCrudPet"
    
    -- Ejecutar Click derecho en la clase -> RUN
    
    *** java/pe/com/baufest/RunnerTestLocal.java
    
>En caso tengas problemas con la version de gradle
>
>File>Setting>Gradle> *[Ubicar la opcion de abajo y cambiarlo]*
>
>User Gradle From: gradle-wrapper.properties/